# Spotify Assistant

> last update on 2021/12/22

## Purpose

This assistant aims to create a local server in order to handle Spotify music management and to visualize several statistics on a given Spotify account

## Requirements

- Python 3.8+
- pip v 21.3+
- spotipy v 2.19+
- sanic 21.9+
